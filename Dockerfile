FROM docker.imp.fu-berlin.de:5000/pub/python-flask

RUN apt-get update \
&& apt-get install -y python3-yamlordereddictloader


EXPOSE 5000

COPY . /srv/yaml_reader

WORKDIR /srv/yaml_reader

CMD ["gunicorn3", "-b", "0.0.0.0:5000", "flask-server:app"]
