#!/usr/bin/python3
from flask import Flask, render_template, request, abort

import yaml
import yamlordereddictloader as yamlODL
import json
from collections import OrderedDict

from os import listdir
from os.path import isfile, join

app = Flask(__name__)
app.config.from_pyfile('jinja-sandbox.ini', silent=True)

DATA = None

filepath = "files/"


#Yaml
def loadyaml(filename):
    file = open(filepath + filename, "r")
    yamldata = file.read()
    global DATA
    DATA = yaml.load(yamldata, Loader=yamlODL.Loader)
    file.close()

#Json
def loadjson(filename):
    file = open(filepath + filename, "r")
    jsondata = file.read()
    global DATA
    DATA = json.loads(jsondata, object_pairs_hook=OrderedDict)
    file.close()

def loadfile(filename):
    print("test")
    print(filename)
    if(filename.split(".")[1] == "yaml" or filename.split(".")[1] == "yml"):
        print("test2")
        loadyaml(filename)
    elif(filename.split(".")[1] == "json"):
        loadjson(filename)

#Load Filenames
def load_filenames():
    filenames = [f for f in listdir(filepath) if isfile(join(filepath, f))]
    filenames = [x for x in filenames if (x.endswith(".json") or x.endswith(".yaml") or x.endswith(".yaml"))]
    return filenames

#Load Nodes
def load_nodes():
    return DATA["nodes"]

#Reload endpoint
def reloadfile(filename):
    loadfile(filename)

#Main page
@app.route('/<file>')
def output(file):
    loadfile(file)
    n = load_nodes()
    h = file
    return render_template('output.html.j2', data=DATA, filenames = n, heading = h, hidereclass = False)

@app.route('/<file>/<startpoint>')
def advanced_output(file, startpoint):
    loadfile(file)
    n = load_nodes()
    d = DATA["nodes"][startpoint]
    h = startpoint
    return render_template('output.html.j2', data=d, filenames = n, heading = h, hidereclass = True)

#Old page
@app.route('/old/<file>')
def old_output(file):
    loadfile(file)
    return render_template('output_backup.html.j2', data=DATA)

#Print Filenames
@app.route('/files')
def print_files():
    filenames = load_filenames()
    output = ""
    for f in filenames:
        output = output + f + "<br>"
    return output

# main
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
