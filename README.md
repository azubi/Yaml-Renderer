# YamlReader

Tool zum strukturierten Anzeigen von yaml Dateien in HTML

## Funktion

flask-server.py mit python3 ausführen, läuft standardmäßig auf 0.0.0.0:5000

Anzuzeigende Dateien liegen im files Ordner, und können mit localhost:5000/<filename> angezeigt werden.

## Docker Anleitung

In die Registry einloggen:  
```
docker login git.imp.fu-berlin.de:5000
```

Das Image herunterladen:  
```
docker pull git.imp.fu-berlin.de:5000/azubi/yaml-renderer:latest
```

Das Image ausführen: (Ports können nach belieben angepasst werden)  
```
docker run -d -p 5000:5000 --name yaml-renderer yaml-renderer
```
