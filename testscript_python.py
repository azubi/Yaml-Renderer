import yaml

def makesomespace(layer):
    output = ""
    for i in range(0,layer):
        output += "  "
    return output

def printdict(dataelement, layer):
    if(isinstance(dataelement, dict)):
        for key in dataelement.keys():
            if(isinstance(dataelement[key], dict) or isinstance(dataelement[key], list)):
                value = "("+len(dataelement[key]).__str__()+")"
            else:
                value = dataelement[key].__str__()
            print(makesomespace(layer) + key + ": " + value)
            printdict(dataelement[key], (layer + 1))
    elif(isinstance(dataelement, list)):
        for item in dataelement:
            if(isinstance(item, dict)):
                printdict(item, layer)
            else:
                print(makesomespace(layer) + "- " + item)



file = open("iris.yaml", "r")
yamldata = file.read()
data = yaml.load(yamldata)
file.close()
printdict(data, 0)
